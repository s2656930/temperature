package nl.utwente.di.temperature;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;


public class Temperature extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
    public void init() throws ServletException {
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Temperature";

    String celsius = request.getParameter("temperature");
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY style=\"background-color: #FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Temperature in Celsius: " +
                    celsius + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                    toFahrenheit(celsius) + "\n" +
                "</BODY></HTML>");
  }

  private double toFahrenheit(String celsius) {
        double s = Double.parseDouble(celsius);
        return (s * 9/5) + 32;
  }
}
